package;

import flixel.FlxSprite;

class Enemy extends FlxSprite
{
	var speed:Float = 25;
	var santa:Santa;

	public function new(x:Float = 0, y:Float = 0, santa:Santa)
	{
		this.santa = santa;
		super(x, y);
		speed = Main.rand.float(30, 50);
		if (Main.rand.bool())
			setPosition(-40, 270);
		else
			setPosition(440, 270);
	}

	override function update(elapsed)
	{
		if (getScreenPosition().x < santa.getScreenPosition().x)
		{
			velocity.x = speed;
		}
		else
			velocity.x = -speed;
		super.update(elapsed);
	}
}
