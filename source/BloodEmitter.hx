package;

import flixel.effects.particles.FlxEmitter;
import flixel.util.helpers.FlxBounds;
import flixel.util.helpers.FlxPointRangeBounds;
import flixel.util.helpers.FlxRangeBounds;

class BloodEmitter extends FlxEmitter
{
	public function new(x:Float = 0, y:Float = 0, Size:Int = 0)
	{
		super(x, y, Size);
		speed = new FlxRangeBounds<Float>(300, 600);
		acceleration = new FlxPointRangeBounds(0, 2000);
		lifespan = new FlxBounds<Float>(1);
	}
}
