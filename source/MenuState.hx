package;

import flixel.math.FlxPoint;
import flixel.text.FlxText.FlxTextAlign;
import openfl.display.StageScaleMode;
import flixel.system.scaleModes.StageSizeScaleMode;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.system.FlxSound;
import flixel.ui.FlxButton;
import flixel.util.FlxAxes;
import flixel.util.FlxColor;

class MenuState extends FlxState
{
	var playButton:FlxButton;
	var background:FlxSprite;
	var intro:FlxSound;
	override public function create()
	{	#if FLX_MOUSE
		if(FlxG.onMobile){

			FlxG.mouse.visible = false;
		}
		#end
		FlxG.stage.scaleMode = StageScaleMode.NO_SCALE;
		// Since Safari does not like .ogg files, I need to use mp3's for HTML5
		// It might be worth it to just use the mp3 format everywhere then....
		#if !html5
		intro = FlxG.sound.load(AssetPaths.intro__ogg);
		#end
		#if html5
		intro = FlxG.sound.load(AssetPaths.intro__mp3);
		#end
		intro.looped = true;
		intro.play();
		super.create();
		background = new FlxSprite();
		background.loadGraphic(AssetPaths.bg2__png, false);
		add(background);
		playButton = new FlxButton(0, 0, "Pelaa", clickPlay);
		playButton.screenCenter();	
		add(playButton);
		var text = new flixel.text.FlxText(0, 50, 0, "Turpakäräjät:\nNenähommat Jouluaattona", 16);
		text.color = FlxColor.RED;
		text.screenCenter(FlxAxes.X);
		add(text);
		
	}
	function clickPlay()
	{
		intro.stop();
		intro.kill();
		FlxG.switchState(new PlayState());
	}

	override public function update(elapsed:Float)
	{
		#if FLX_KEYBOARD
		if (FlxG.keys.anyPressed([ENTER]))
		{
			clickPlay();
		}
		#end
		super.update(elapsed);
	}
}
