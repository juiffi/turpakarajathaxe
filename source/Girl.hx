package;

import flixel.FlxObject;

class Girl extends Enemy
{
	public function new(x:Float = 0, y:Float = 0, santa:Santa)
	{
		super(x, y, santa);
		loadGraphic(AssetPaths.girlsheet__png, true, 24, 30);
		setFacingFlip(FlxObject.LEFT, true, false);
		setFacingFlip(FlxObject.RIGHT, false, false);
		animation.add("run", [5, 6, 7, 8], 10, false);

		speed = speed * 0.8;
		setSize(14, 20);
		offset.set(5, 4);
		health = 100;
	}

	override function update(elapsed)
	{
		super.update(elapsed);
		animation.play("run");
		checkDirection();
	}

	function checkDirection()
	{
		if (getScreenPosition().x < santa.getScreenPosition().x)
			facing = FlxObject.RIGHT;
		else
			facing = FlxObject.LEFT;
	}
}
