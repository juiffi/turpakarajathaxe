package;

import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.system.FlxSound;
import flixel.util.FlxColor;

class Santa extends FlxSprite
{
	static inline var SPEED:Float = 100;

	var jumping:Bool = false;
	var left:Bool = false;
	var right:Bool = false;
	var xPressed:Bool=false;

	var zPressed:Bool=false;
	public var nyrkkiBox:FlxSprite;

	static inline var INITIAL_JUMPFORCE:Float = 300;

	var jumpForce:Float = 0;
	var attacking:Bool = false;
	var whiff:Bool = false;
	var attackCooldown:Float = 0;
	var attackCooldownMax:Float = 0.25;
	var attackHand:Bool = false;

	var canHurt:Bool = false;
	var hitSound:FlxSound;

	public var score:Float = 0;
	public var levelUp:Int = 1000;

	public function giveScore(i:Float)
	{
		score += i;
		if (score >= levelUp)
		{
			attackCooldownMax = (attackCooldownMax * 0.9);
			animation.getByName("attackL").frameRate = animation.getByName("attackL").frameRate / 0.9;
			animation.getByName("attackR").frameRate = animation.getByName("attackR").frameRate / 0.9;

			levelUp = levelUp * 2;
		}
	}

	public function stopAttack()
	{
		whiff = true;
	}

	public function getCanHurt():Bool
	{
		return canHurt;
	}

	public function new(x:Float = 0, y:Float = 0)
	{
		
		super(x, y);
		nyrkkiBox = new FlxSprite();
		nyrkkiBox.setSize(22, 8);
		nyrkkiBox.makeGraphic(22, 8, FlxColor.RED);
		#if !html5
		hitSound = FlxG.sound.load(AssetPaths.hit__ogg);
		#end
		#if html5
		hitSound = FlxG.sound.load(AssetPaths.hit__mp3);
		#end
		loadGraphic(AssetPaths.santasheet__png, true, 96, 96);
		setSize(16, 28);
		offset.set(37, 36);
		var perkele:Int = 1;
		animation.add("idle", [0, 1 * perkele, 2 * perkele, 3 * perkele, 4 * perkele], 10, false);
		animation.add("run", [
			9 * perkele,
			10 * perkele,
			11 * perkele,
			12 * perkele,
			13 * perkele,
			14 * perkele,
			15 * perkele,
			16 * perkele
		], 10, false);
		animation.add("attackL", [18 * perkele, 19 * perkele, 20 * perkele, 21 * perkele, 22 * perkele], 20, false);
		animation.add("attackR", [27 * perkele, 28 * perkele, 29 * perkele, 30 * perkele, 31 * perkele], 20, false);
		setFacingFlip(FlxObject.LEFT, true, false);
		setFacingFlip(FlxObject.RIGHT, false, false);
	}

	override function update(elapsed:Float)
	{
		if (facing == FlxObject.LEFT)
			offset.set(42, 36);
		else
			offset.set(37, 36);

		if (facing == FlxObject.RIGHT)
			nyrkkiBox.setPosition(getScreenPosition().x + 8, getScreenPosition().y + 10);
		else
			nyrkkiBox.setPosition(getScreenPosition().x - 13, getScreenPosition().y + 10);

		handleJump();
		handleRunning();
		handleAttack(elapsed);
		decideAnimation();
		super.update(elapsed);
	}

	function handleJump()
	{
		
		
		#if mobile
		//xPressed = PlayState.virtualPad.buttonB.pressed;
		#end
		#if FLX_KEYBOARD
		xPressed =FlxG.keys.anyPressed([X]);		 
		#end

		// If we have landed, stop jumping and reset position to ground level
		if (getPosition().y >= 262)
			{
				jumping = false;
				velocity.y = 0;
				setPosition(getPosition().x, 262);
			}
			// If X is pressed, jump!
			if (xPressed)
			{
				if (getScreenPosition().y >= 262)
				{
					jumping = true;
					jumpForce = INITIAL_JUMPFORCE;
				}
			}
			// If we are jumping, give the player velocity
			if (jumping)
			{
				velocity.y = (-jumpForce);
				jumpForce = jumpForce - Main.GRAVITY;
			}
	}

	function handleRunning()
	{
		#if FLX_KEYBOARD
		left = FlxG.keys.anyPressed([LEFT]);
		right = FlxG.keys.anyPressed([RIGHT]);
		#end
		#if FLX_TOUCH
		if(FlxG.onMobile){
		left  = left || PlayState.virtualPad.buttonLeft.pressed;
		right = right || PlayState.virtualPad.buttonRight.pressed;
		}
		#end
		if (left && right)
		{
			left = right = false;
		}
		if (left || right)
		{
			if (left)
			{
				if (getScreenPosition().x > 0)
				{
					velocity.x = (-SPEED);
				}
				else
				{
					velocity.x = 0;
				}
				facing = FlxObject.LEFT;
			}
			if (right)
			{

								if (getScreenPosition().x < 400 - 28)
					velocity.x = SPEED;
					else
					{
						velocity.x = 0;
					}
					facing = FlxObject.RIGHT;
			}
		}
		else
		{
			velocity.x = 0;
		}
	}

	function handleAttack(elapsed:Float)
	{
				#if FLX_TOUCH
				if(FlxG.onMobile){
				zPressed = PlayState.virtualPad.buttonA.pressed;
				}
                #end
                #if FLX_KEYBOARD
                zPressed = zPressed ||FlxG.keys.anyPressed([Z]);
                #end

		// Tähän timeri
		if (attackCooldown > 0)
		{
			attackCooldown -= elapsed;
		}
		if (attackCooldown <= 0 && zPressed)
		{
			hitSound.play(true);
			whiff = false;
			attacking = true;
			attackCooldown = attackCooldownMax;
			if (attackHand)
			{
				attackHand = false;
			}
			else
			{
				attackHand = true;
			}
		}

		if (animation.curAnim != null)
		{
			if (attacking && animation.curAnim.curFrame > 0 && animation.curAnim.curFrame < animation.curAnim.numFrames - 1 && !whiff)
			{
				canHurt = true;
			}
			else
			{
				canHurt = false;
			}
		}
		zPressed = false;
	}

	function decideAnimation()
	{
		if (attacking)
		{
			if (attackHand)
			{
				animation.play("attackL");
			}
			else
			{
				animation.play("attackR");
			}
			if (animation.curAnim != null && animation.curAnim.curFrame == 4)
			{
				attacking = false;
			}
		}
		else if (left || right)
		{
			animation.play("run");
		}
		else
		{
			animation.play("idle");
		}
		left = false;
		right = false;
	}
}
