package;

import openfl.filters.BitmapFilter;
import flixel.FlxG;
import flixel.FlxGame;
import flixel.math.FlxRandom;
import flixel.system.FlxSound;
import openfl.display.FPS;
import openfl.display.Sprite;

class Main extends Sprite
{
	public static inline var GRAVITY:Float = 10;
	public static var rand:FlxRandom = new FlxRandom();
	
	public function new()
	{	
		super();
		addChild(new FlxGame(400, 300, MenuState, 4, 60, 60, true));
		//var fps:FPS = new FPS(0, 0, 0xffffff);
		//addChild(fps);
	}
}
