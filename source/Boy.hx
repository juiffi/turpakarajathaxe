package;

import flixel.FlxObject;

class Boy extends Enemy
{
	var jumpCharge:Float;
	var jumpChargeMax:Float = 1;
	var cooldown:Float;
	var jumpThreshold:Int;
	var shake:Float;
	var maxShake:Float = 0.05;
	var shakedir:Int;
	var jumping:Bool;
	var jumpForce:Float = 0;
	var jumpSpeedMultiplier:Float = 2.5;
	var origSpeed:Float;
	var jumpSpeed:Float = 0;

	static inline var INITIAL_JUMPFORCE:Float = 175;

	public function new(x:Float = 0, y:Float = 0, santa:Santa)
	{
		super(x, y, santa);
		loadGraphic(AssetPaths.boysheet__png, true, 24, 30);
		setFacingFlip(FlxObject.LEFT, true, false);
		setFacingFlip(FlxObject.RIGHT, false, false);
		animation.add("run", [5, 6, 7, 8], 10, false);

		jumpThreshold = Main.rand.int(20, 75);
		jumpCharge = jumpChargeMax;

		shake = maxShake;
		origSpeed = speed;
		jumpSpeed = origSpeed * jumpSpeedMultiplier;
		cooldown = 0;
		setSize(14, 20);
		offset.set(5, 4);
		health = 200;
	}

	override function update(elapsed:Float)
	{
		handleJump(elapsed);
		// Jos maassa -> jumping false

		// Jos kriteerit kohdallaan, hyppää

		// Jos jumping -> tee jutut
		super.update(elapsed);
		checkDirection();
		animation.play("run");
	}

	function handleJump(elapsed:Float)
	{
		// If we have landed, stop jumping and reset position to ground level
		if (getPosition().y > 270)
		{
			speed = origSpeed;
			jumping = false;
			velocity.y = 0;
			setPosition(getPosition().x, 270);
			jumpCharge = jumpChargeMax;
		}
		// If X is pressed, jump!
		if (Math.abs(getScreenPosition().x - santa.getScreenPosition().x) < jumpThreshold && getScreenPosition().y >= 270 && cooldown <= 0)
		{
			if (getScreenPosition().y >= 270)
			{
				jumping = true;
				jumpForce = INITIAL_JUMPFORCE;
				cooldown = 2;
			}
		}
		// If we are jumping, give the player velocity
		if (jumping)
		{
			jumpCharge -= elapsed;
			if (jumpCharge <= 0)
			{
				speed = jumpSpeed;
				velocity.y = (-jumpForce);
				jumpForce = jumpForce - Main.GRAVITY;
			}
			else
			{
				shake -= elapsed;
				if (shake <= 0)
				{
					shake = maxShake;
					speed = speed * -1;
				}
			}
		}
		else
			speed = origSpeed;
		if (cooldown > 0)
		{
			cooldown -= elapsed;
		}
	}

	function checkDirection()
	{
		if (getScreenPosition().x < santa.getScreenPosition().x)
			facing = FlxObject.RIGHT;
		else
			facing = FlxObject.LEFT;
	}
}
