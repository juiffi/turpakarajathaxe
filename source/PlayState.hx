package;

import flixel.ui.FlxVirtualPad;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.effects.particles.FlxEmitter;
import flixel.effects.particles.FlxParticle;
import flixel.group.FlxSpriteGroup;
import flixel.system.FlxSound;
import flixel.ui.FlxButton;
import flixel.util.FlxAxes;
import flixel.util.FlxColor;

class PlayState extends FlxState
{
	#if FLX_TOUCH
	public static var virtualPad:FlxVirtualPad;
	#end
	var santa:Santa;
	var gameOver:Bool;
	var background:FlxSprite;
	var enemies:FlxSpriteGroup = new FlxSpriteGroup();
	var newGameButton:FlxButton;
	var deathSound:FlxSound;
	var music:FlxSound;
	var difficulty = 0.1;
	var blood:BloodEmitter;
	var bloodSize:Int;
	var scoreText:flixel.text.FlxText;
	var lvlText:flixel.text.FlxText;
	var intro:FlxSound;

	override public function create()
	{
		#if !html5
		intro = FlxG.sound.load(AssetPaths.intro__ogg);
		#end
		#if html5
		intro = FlxG.sound.load(AssetPaths.intro__mp3);
		#end
		// Hide the cursor while playing
		#if FLX_MOUSE
		FlxG.mouse.visible = false;
		#end

		gameOver = false;
		#if html5
		music = FlxG.sound.load(AssetPaths.play__mp3);
		#end
		#if !html5
		music = FlxG.sound.load(AssetPaths.play__ogg);
		#end
		music.looped = true;
		music.play();
		#if !html5
		deathSound = FlxG.sound.load(AssetPaths.explo__ogg);
		#end
		#if html5
		deathSound = FlxG.sound.load(AssetPaths.explo__mp3);
		#end

		super.create();

		background = new FlxSprite();
		background.loadGraphic(AssetPaths.bg2__png, false);
		add(background);

		santa = new Santa(0, 300 - 18);
		santa.screenCenter(FlxAxes.X);
		add(santa);

		enemies.add(new Boy(0, 0, santa));
		enemies.add(new Girl(0, 0, santa));
		enemies.add(new Girl(0, 0, santa));

		for (enemy in enemies)
		{
			add(enemy);
		}
		
		bloodSize = 100;

		// max amount of blood particles gets set higher, if the build is a native cpp build
		#if cpp
		bloodSize = 200;
		#end

		blood = new BloodEmitter(0, 0, bloodSize);
		addBlood(bloodSize);
		add(blood);
		blood.launchMode = FlxEmitterMode.CIRCLE;

		scoreText = new flixel.text.FlxText(0, 0, 0, "Score: " + santa.score, 16);
		scoreText.color = FlxColor.RED;
		add(scoreText);

		lvlText = new flixel.text.FlxText(0, 24, 0, "Level up at: " + santa.levelUp, 16);
		lvlText.color = FlxColor.RED;

		add(lvlText);
		// If i dont add the nyrkkibox (hitbox of the fist) to the level, it's position is not updated correctly on android
		add(santa.nyrkkiBox);
		santa.nyrkkiBox.visible = false;
		// If touch input supported AND on mobile -> then add pad
		// HTML5 supports touch input, but i dont want to add the virtualPad on desktop browsers!
		#if FLX_TOUCH
		if(FlxG.onMobile){
			virtualPad = new FlxVirtualPad(LEFT_RIGHT,A);
			virtualPad.setPosition(virtualPad.getScreenPosition().x,-120);
			add(virtualPad);
		}
		#end

	}
	function addBlood(amount:Int)
	{
		var i:Int = 0;

		var bloodParticle:FlxParticle;
		while (i < amount)
		{
			bloodParticle = new FlxParticle();
			bloodParticle.makeGraphic(3, 3, FlxColor.RED);
			bloodParticle.visible = false; // Make sure the particle doesn't show up at (0, 0)
			blood.add(bloodParticle);
			i++;
		}
	}

	override public function update(elapsed:Float)
	{
		scoreText.text = "Score: " + santa.score;
		lvlText.text = "Level up at: " + santa.levelUp;
		playerAttack();
		playerDeath();
		super.update(elapsed);
		#if FLX_KEYBOARD
		if (gameOver && FlxG.keys.anyPressed([ENTER]))
		{
			newGame();
		}
		#end
	}

	function playerDeath()
	{
		if (FlxG.overlap(santa, enemies))
		{
			#if FLX_MOUSE
			if(!FlxG.onMobile)
				FlxG.mouse.visible = true;
			#end
			blood.setPosition(santa.getPosition().x, santa.getPosition().y);
			blood.start(true, 0, bloodSize);
			gameOver = true;
			music.stop();
			music.kill();
			deathSound.play();
			santa.kill();

			newGameButton = new FlxButton(0, 200, "Uusi Peli", newGame);
			newGameButton.screenCenter(FlxAxes.X);
			newGameButton.setGraphicSize(52, 26);
			add(newGameButton);

			var text = new flixel.text.FlxText(0, 42, 0, "Kuolit!", 64);
			text.screenCenter(FlxAxes.X);
			text.color = FlxColor.RED;
			add(text);

			intro.play(true);
		}
	}

	function newGame()
	{
		intro.stop();
		intro.kill();
		FlxG.switchState(new PlayState());
	}

	function playerAttack()
	{
		if (!gameOver && santa.getCanHurt())
		{
			var dod:FlxSprite = null;
			for (enemy in enemies)
			{
				if (santa.getCanHurt() && FlxG.overlap(santa.nyrkkiBox, enemy))
				{
					if (dod != null)
					{
						var enemyDist:Float = Math.abs(enemy.getScreenPosition().x - santa.getScreenPosition().x);
						var dodDist:Float = Math.abs(dod.getScreenPosition().x - santa.getScreenPosition().x);
						if (enemyDist < dodDist)
							dod = enemy;
					}
					else
						dod = enemy;
				}
			}
			if (dod != null)
			{
				santa.stopAttack();
				santa.giveScore(dod.health);
				blood.setPosition(dod.getPosition().x, dod.getPosition().y);
				dod.kill();
				addBlood(Math.floor(bloodSize/5));
				blood.start(true, 0, Math.floor((bloodSize/5)));
				if (Main.rand.bool())
					dod = new Boy(0, 0, santa);
				else
					dod = new Girl(0, 0, santa);
				add(enemies.add(dod));
				difficulty += 0.001;
				if (Main.rand.float(0, 1) < difficulty)
				{
					if (Main.rand.bool())
						add(enemies.add(new Boy(0, 0, santa)));
					else
						add(enemies.add(new Boy(0, 0, santa)));
				}
			}
		}
	}
}
