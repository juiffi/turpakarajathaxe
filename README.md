# TurpakarajatHaxe

Turpakäräjät: Nenähommat Jouluaattona Haxella toteutettuna. Haxella helpompi kääntää ohjelma mobiilialustoille ja selaimille.  
Seuraavaksi voisi lisätä vaikka tuen gamepadeille tms...

### [HTML5 Demo](http://kouluhommatjuhor.ddns.net:3050/turpakarajat)

# Koonti

Varmista, että Haxe on asennettu:  
https://haxe.org/download/  
Sitten asenna HaxeFlixel  
```
haxelib install lime  
haxelib install openfl  
haxelib install flixel  
haxelib run lime setup flixel  
haxelib run lime setup  
haxelib install flixel-tools  
haxelib run flixel-tools setup  
```

Sitten perus setit:

```
git clone https://gitlab.com/juiffi/turpakarajathaxe.git
cd turpakarajathaxe
lime test TARGET_PLATFORM
```
Korvaa TARGET_PLATFORM alustalla, jolle haluat kääntää. Esim.  
```
lime test android
lime test linux
lime test html5
lime test mac
lime test windows
```

## Santa Claus art by Elthen
* [https://opengameart.org/content/pixel-art-santa-sprites](https://opengameart.org/content/pixel-art-santa-sprites)
* [https://www.patreon.com/elthen](https://www.patreon.com/elthen)

## Enemy sprites by iPixl
* [https://ipixl.itch.io/pixel-art-animated-characters-pack](https://ipixl.itch.io/pixel-art-animated-characters-pack)

## Sounds from freesound
* [https://freesound.org/people/gprosser/sounds/412863/](https://freesound.org/people/gprosser/sounds/412863/)
* [https://freesound.org/people/tommccann/sounds/235968/](https://freesound.org/people/tommccann/sounds/235968/)

## Alkuperäinen olcPGE C++ toteutus
[https://gitlab.com/juiffi/turpakarajatjoulu](https://gitlab.com/juiffi/turpakarajatjoulu)
